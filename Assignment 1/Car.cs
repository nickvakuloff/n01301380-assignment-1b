﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1
{
    public class Car
    {
        public string name;
        public string color;
        public List<string> a_stuff;
        public string i_color;

        public Car(string n, string c, List<string> s, string ic)
        {
            name = n;
            color = c;
            a_stuff = s;
            i_color = ic;
        }
    }
}