﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1
{
    public class Order
    {
        public Car car;
        public Rent rent;
        public Customer customer;

        public Order(Car c, Rent r, Customer cs)
        {
            car = c;
            rent = r;
            customer = cs;
        }

        public string PrintReceipt()
        {
            string receipt = "Order Receipt:<br/>";
            receipt += "Your total is :" + CalculateOrder().ToString() + "<br/>";
            receipt += "First Name: " + customer.CustomerFirstName + "<br/>";
            receipt += "Last Name: " + customer.CustomerLastName + "<br/>";
            receipt += "Phone Number: " + customer.CustomerPhone + "<br/>";

            receipt += "Car Name: " + car.name +"<br/>";
            receipt += "Car Color: " + car.color + "<br/>";
            receipt += "Car Stuff: " + String.Join(" ", car.a_stuff.ToArray()) + "<br/>";
            receipt += "Interior color: " + car.i_color + "<br/>";

            receipt += "Rent for " + rent.time + " days";

            return receipt;
        }

        public double CalculateOrder()
        {
            double total = 0;
            if (car.name == "Ford")
            {
                total = 300;
            }else if(car.name == "Mersedes")
            {
                total = 500;
            }
            else if (car.name == "Mazda")
            {
                total = 350;
            }
            else if (car.name == "Honda")
            {
                total = 350;
            }
            else if (car.name == "BMW")
            {
                total = 500;
            }
            else if (car.name == "Shuttle")
            {
                total = 100000;
            }
            else if (car.name == "Dragon")
            {
                total = 10000;
            }
            else if (car.name == "Unicorn")
            {
                total = 1000000;
            }
            else if (car.name == "St")
            {
                total = 5000;
            }
            else if (car.name == "Alladin")
            {
                total = 50000;
            }

            total = total * rent.time + car.a_stuff.Count() * 14;
            return total;
        }
    }
}