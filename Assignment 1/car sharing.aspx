﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="car sharing.aspx.cs" Inherits="Assignment_1.n01301380" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
  <form id="form1" runat="server">
        <div>
            <h1 id="info" runat="server">Car Sharing</h1>
            <asp:TextBox runat="server" ID="client_fname" placeholder="Enter your first name"/>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a first Name" ControlToValidate="client_fname" ID="validatorFirstName"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="client_lname" placeholder="Enter your last name"/>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a last Name" ControlToValidate="client_lname" ID="validatorLastName"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="client_phone" placeholder="Enter your phone number"/>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a phone number" ControlToValidate="client_phone" ID="validatorPhoneNumber"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid Phone number" ControlToValidate="client_phone" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" ></asp:RegularExpressionValidator>
            <!--stolen from https://stackoverflow.com/questions/18585613/how-do-you-validate-phone-number-in-asp-net -->
            <br />
            <asp:Label Text="Choose a car" runat="server" />
            <br />
            <asp:DropDownList runat="server" ID="carName">
                <asp:ListItem Value="Ford" Text="Ford" />
                <asp:ListItem Value="Mercedes" Text="Mercedes" />
                <asp:ListItem Value="Mazda" Text="Mazda" />
                <asp:ListItem Value="Honda" Text="Honda" />
                <asp:ListItem Value="BMW" Text="BMW" />
                <asp:ListItem Value="Shuttle" Text="Space Shuttle" />
                <asp:ListItem Value="Dragon " Text="Dragon" />
                <asp:ListItem Value="Unicorn" Text="Unicorn" />
                <asp:ListItem Value="St" Text="Siege Tower" />
                <asp:ListItem Value="Alladin" Text="Flying Carpet" />
            </asp:DropDownList>
            <br />
            <asp:Label Text="Number of days:" runat="server" />
            <br />
            <asp:TextBox runat="server" ID="n_days" placeholder="Number of Days"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="n_days" Type="Double" MinimumValue="1" MaximumValue="30" ErrorMessage="You can't rent a car more than 30 days"></asp:RangeValidator>
            <br />
            <asp:Label Text="Choose the color:" runat="server" />
            <br />
            <asp:RadioButtonList runat="server" ID="carColor">
                <asp:ListItem Text="Red"  runat="server" GroupName="color"/>  
                <asp:ListItem Text="Green"  runat="server" GroupName="color"/>   
                <asp:ListItem Text="Blue" runat="server" GroupName="color"/> 
           </asp:RadioButtonList>
            <div id="a_stuff" runat="server">
                <asp:Label Text="Additional stuff:" runat="server" />
                <br />
                <asp:CheckBox Text="USB charger" ID="stuff1" runat="server" /> 
                <asp:CheckBox Text="Dash cam" ID="stuff2" runat="server" />
                <asp:CheckBox Text="Escort Max 360" ID="stuff3" runat="server" />
            </div> 
            <asp:Label Text="Interior color:" runat="server" />
            <br />
            <asp:RadioButtonList runat="server" ID="interiorColor">
                <asp:ListItem Text="Black" runat="server" GroupName="i_color"/>
                <asp:ListItem Text="White" runat="server" GroupName="i_color"/>
                <asp:ListItem Text="Grey" runat="server" GroupName="i_color"/>
             </asp:RadioButtonList>
            <asp:Button runat="server" ID="myButton" Text="Submit" OnClick="Order"/>
        </div>
      <br />
      <div runat="server" id="OrderRes"></div>
    </form>
</body>
</html>
