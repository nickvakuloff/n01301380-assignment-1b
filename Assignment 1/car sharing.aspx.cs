﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment_1
{
    public partial class n01301380 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected void Order(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            info.InnerHtml = "Thank you for your order!";

            //Car
            string name = carName.SelectedItem.Value.ToString();
            string color = carColor.SelectedItem.Text;
            List<string> carStuff = new List<string>();
            string icolor = interiorColor.SelectedItem.Text;


            foreach (Control control in a_stuff.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox stuff = (CheckBox)control;
                    if (stuff.Checked)
                    {
                        carStuff.Add("<br/>"+ "-" + stuff.Text);
                    }

                }
            }

            Car newcar = new Car(name, color, carStuff, icolor);

            //Rent           

            double ndays = double.Parse(n_days.Text);
            Rent newrent = new Rent(ndays);

            //Customer

            string cfname = client_fname.Text.ToString();
            string clname = client_lname.Text.ToString();
            string cphone = client_phone.Text.ToString();

            Customer newcustomer = new Customer();
            newcustomer.CustomerFirstName = cfname;
            newcustomer.CustomerLastName = clname;
            newcustomer.CustomerPhone = cphone;

            Order neworder = new Order(newcar, newrent, newcustomer);

            OrderRes.InnerHtml = neworder.PrintReceipt();
        }

        

    }
}