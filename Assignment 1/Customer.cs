﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment_1
{
    public class Customer
    {
        private string customerFirstName;
        private string customerLastName;
        private string customerPhone;

        public Customer()
        {

        }
        public string CustomerFirstName
        {
            get { return customerFirstName; }
            set { customerFirstName = value; }
        }
        public string CustomerLastName
        {
            get { return customerLastName; }
            set { customerLastName = value; }
        }
        public string CustomerPhone
        {
            get { return customerPhone; }
            set { customerPhone = value; }
        }
    }
}